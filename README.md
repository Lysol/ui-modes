# UI modes

Various gameplay and UI tweaks including menu hotkeys, no pause when menus are up and much more.

**Requires OpenMW 0.49 or newer!**

Features (each feature can be separately enabled or disabled in mod settings):
- Separate UI pages for Map, Spells, Stats, Journal
- Skyrim-like widget to choose the page (controls: mouse, movement keys, controller DPad)
- Key bindings 'M' (Map), 'C' (Stats), 'I' (Inventory)
- Attack button (left mouse click) automatically prepares the last used weapon/spell.
- An option to not pause the game in inventory and dialogs.
- Ability to rotate 3rd person camera in inventory mode and during dialogs (controls: left/right arrow keys, controller right stick).

#### Credits

Author: [Petr Mikheev](https://gitlab.com/ptmikheev)

#### Web

[Project Home](https://modding-openmw.gitlab.io/ui-modes/)

<!-- [Nexus Mods](https://www.nexusmods.com/morrowind/mods/#TODO) -->

[Source on GitLab](https://gitlab.com/modding-openmw/ui-modes)


#### Installation

[This short video](https://www.youtube.com/watch?v=xzq_ksVuRgc) demonstrates the whole process in under two minutes.

1. Download the zip from [this URL](https://modding-openmw.gitlab.io/ui-modes/) (**OpenMW 0.49.0 or newer is required!**)
1. Extract the contents to a location of your choosing, examples below:

        # Windows
        C:\games\OpenMWMods\UserInterface\ui-modes

        # Linux
        /home/username/games/OpenMWMods/UserInterface/ui-modes

        # macOS
        /Users/username/games/OpenMWMods/UserInterface/ui-modes
1. Add the appropriate data path to your `openmw.cfg` file, for example:

        data="C:\games\OpenMWMods\UserInterface\ui-modes"
1. Add the following to your load order in `openmw.cfg` or enable it via OpenMW-Launcher :

        content=UiModes.omwscripts
1. The following window dimensions are recommended (add this to your `settings.cfg` file, replacing any pre-existing `[Windows]`section):

        [Windows]
        inventory h = 0.70
        inventory w = 0.47
        inventory x = 0.02
        inventory y = 0.18
        inventory barter h = 0.70
        inventory barter w = 0.47
        inventory barter x = 0.02
        inventory barter y = 0.18
        inventory container h = 0.70
        inventory container w = 0.47
        inventory container x = 0.02
        inventory container y = 0.18
        stats h = 0.70
        stats w = 0.47
        stats x = 0.51
        stats y = 0.18
        spells h = 0.70
        spells w = 0.47
        spells x = 0.51
        spells y = 0.18
        barter h = 0.70
        barter w = 0.47
        barter x = 0.51
        barter y = 0.18
        container h = 0.70
        container w = 0.47
        container x = 0.51
        container y = 0.18
        map h = 0.8
        map w = 0.8
        map x = 0.1
        map y = 0.1

