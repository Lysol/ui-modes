## UI modes Changelog

#### Version 1.1

- Fixed bug with locked character direction after closing inventory
- Window can be chosen by mouse movement when right mouse button is holded

#### Version 1.0

Initial version of the mod.

<!-- [Download Link](https://gitlab.com/modding-openmw/ui-modes/-/packages/#TODO) -->
