local storage = require('openmw.storage')
local I = require('openmw.interfaces')

local settings = storage.playerSection('SettingsUiModes')

local M = {}

function M.subscribe(callback)
    settings:subscribe(callback)
end

local function boolSetting(key, default)
    M[key] = function() return settings:get(key) end
    return {
        key = key,
        renderer = 'checkbox',
        name = key,
        description = key..'Description',
        default = default,
    }
end
local function floatSetting(key, default)
    M[key] = function() return settings:get(key) end
    return {
        key = key,
        renderer = 'number',
        name = key,
        description = key..'Description',
        default = default,
    }
end

I.Settings.registerPage({
  key = 'UiModes',
  l10n = 'UiModes',
  name = 'UiModes',
  description = 'UiModesDescription',
})

I.Settings.registerGroup({
    key = 'SettingsUiModes',
    page = 'UiModes',
    l10n = 'UiModes',
    name = 'Settings',
    permanentStorage = true,
    settings = {
        boolSetting('uiModesShortcuts', true),
        boolSetting('uiModesWidget', true),
        boolSetting('autoDrawWeapon', true),
        boolSetting('rotateCameraInInventory', true),
        boolSetting('rotateCameraInDialogs', true),
        floatSetting('rotateCameraSpeed', 2.0),
        boolSetting('inventoryDontPause', true),
        boolSetting('dialogsDontPause', true),
    },
})

return M

